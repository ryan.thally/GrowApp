//
//  CareActivityList.swift
//  Grow iOS
//
//  Created by Ryan Thally on 7/13/20.
//  Copyright © 2020 Ryan Thally. All rights reserved.
//

import SwiftUI

struct CareActivityList: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct CareActivityList_Previews: PreviewProvider {
    static var previews: some View {
        CareActivityList()
    }
}
